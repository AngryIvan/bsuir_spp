package com.bsuir.awp.AWPServer.service;

import com.bsuir.awp.AWPServer.domain.News;

import java.util.List;

public interface NewsService {

    News getById(Long id);

    News addNews(News news);

    List<News> getAllNews();

    News updateNews(News news, Long id);

    void deleteNews(Long id);
}
