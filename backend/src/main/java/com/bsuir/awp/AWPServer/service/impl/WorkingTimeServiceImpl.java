package com.bsuir.awp.AWPServer.service.impl;


import com.bsuir.awp.AWPServer.domain.WorkingTime;
import com.bsuir.awp.AWPServer.repository.WorkingTimeRepository;
import com.bsuir.awp.AWPServer.service.WorkingTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkingTimeServiceImpl implements WorkingTimeService {


    @Autowired
    private WorkingTimeRepository repository;

    @Override
    public WorkingTime addTime(WorkingTime time) {
        return repository.save(time);
    }

    @Override
    public List<WorkingTime> getAllTime() {
        return repository.findAll();
    }
}
