package com.bsuir.awp.AWPServer.service.impl;

import com.bsuir.awp.AWPServer.domain.Supplier;
import com.bsuir.awp.AWPServer.repository.SupplierRepository;
import com.bsuir.awp.AWPServer.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierRepository repository;

    @Override
    public Supplier getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public Supplier addSupplier(Supplier supplier) {
        if(!repository.existsByName(supplier.getName())) {
            return repository.save(supplier);
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public List<Supplier> getAllSuppliers() {
        return repository.findAll();
    }

    @Override
    public Supplier updateSupplier(Supplier newSupplier, Long id) {
        if(!repository.existsByName(newSupplier.getName())) {
            return repository.findById(id).map(supplier -> {
                supplier.setName(newSupplier.getName());
                return repository.save(supplier);
            }).orElseGet(() -> {
                newSupplier.setId(id);
                return repository.save(newSupplier);
            });
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public void deleteSupplier(Long id) {
        repository.findById(id).ifPresent(supplier -> repository.delete(supplier));
    }
}
