package com.bsuir.awp.AWPServer.service.impl;

import com.bsuir.awp.AWPServer.domain.News;
import com.bsuir.awp.AWPServer.repository.NewsRepository;
import com.bsuir.awp.AWPServer.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsRepository repository;

    @Override
    public News getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public News addNews(News news) {
        if(!repository.existsByTitle(news.getTitle())) {
            return repository.save(news);
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public List<News> getAllNews() {
        return repository.findAll();
    }

    @Override
    public News updateNews(News newNews, Long id) {
        if(!repository.existsByTitle(newNews.getTitle())) {
            return repository.findById(id).map(news -> {
                news.setTitle(newNews.getTitle());
                news.setDescription(newNews.getDescription());
                return repository.save(news);
            }).orElseGet(() -> {
                newNews.setId(id);
                return repository.save(newNews);
            });
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public void deleteNews(Long id) {
        repository.findById(id).ifPresent(news -> repository.delete(news));
    }
}
