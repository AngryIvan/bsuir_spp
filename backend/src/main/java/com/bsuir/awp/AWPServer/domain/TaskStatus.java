package com.bsuir.awp.AWPServer.domain;

public enum TaskStatus {
    TODO_STATUS,
    INPROGRESS_STATUS,
    DONE_STATUS,
}
