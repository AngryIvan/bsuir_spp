package com.bsuir.awp.AWPServer.dto;

import com.bsuir.awp.AWPServer.domain.RoleName;
import lombok.*;

import java.util.List;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {
    private Long id;

    @Size(min = 5, max = 15, message = "Min length should be 5 and max length should 15")
    private String login;

    @Size(min = 7, message = "Min length should be 7")
    private String password;

    @Pattern(regexp = "^(.+)@(.+)$", message = "Incorrect email")
    private String email;

    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$", message = "Incorrect phone number")
    private String phone;
    private List<RoleName> roles;
}
