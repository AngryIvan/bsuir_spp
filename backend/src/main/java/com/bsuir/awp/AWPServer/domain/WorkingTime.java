package com.bsuir.awp.AWPServer.domain;

import com.sun.istack.internal.Interned;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "working_time")
public class WorkingTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="employee_id", nullable=false)
    private Employee employeeId;

    @CreationTimestamp
    @Column(name = "create_date", updatable = false)
    private LocalDateTime workDate;

    @Column(name = "time_seconds")
    private int timeSeconds;

}
