package com.bsuir.awp.AWPServer.domain;

import lombok.Data;

import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products")
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    public Long id;

    @Size(min = 5, max = 25, message = "Min length should be 5 and max should be 25")
    @NotEmpty(message = "Product name can't be empty")
    @Column(length = 50, nullable = false, unique = true)
    private String name;

    @Column(length = 5, nullable = false)
    private double cost;

    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade=CascadeType.ALL)
    private Set<Order> orders;
}
