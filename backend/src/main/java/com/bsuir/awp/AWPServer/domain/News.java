package com.bsuir.awp.AWPServer.domain;

import lombok.Data;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Data
@Table(name = "news")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "news_id")
    public Long id;

    @Size(min = 5, max = 25, message = "Min length should be 5 and max should be 25")
    @Column(length = 25, nullable = false, unique = true)
    private String title;

    @Size(min = 25, max = 2000, message = "Min length should be 25 and max should be 2000")
    @Column(length = 2000, nullable = false)
    private String description;

    @CreationTimestamp
    @Column(name = "create_date", updatable = false)
    private LocalDateTime createDate;
}
