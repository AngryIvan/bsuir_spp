package com.bsuir.awp.AWPServer.repository;

import com.bsuir.awp.AWPServer.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findByEmployee_id(Long employeeId);
}
