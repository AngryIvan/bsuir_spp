package com.bsuir.awp.AWPServer.domain;

import lombok.Data;

import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "actions")
@Data
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "action_id")
    public Long id;

    @Size(min = 5, max = 50, message = "Min length should be 5 and max should be 50")
    @Column(length = 50, nullable = false, unique = true)
    private String name;

    @Column(length = 5, nullable = false)
    private Double cost;

    @JsonIgnore
    @OneToMany(mappedBy = "action", cascade= CascadeType.ALL)
    private Set<Order> orders;
}
