package com.bsuir.awp.AWPServer.repository;

import com.bsuir.awp.AWPServer.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findByLogin(String login);
    boolean existsByLogin(String login);
    boolean existsByEmail(String email);
}