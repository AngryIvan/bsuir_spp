package com.bsuir.awp.AWPServer.service;

import com.bsuir.awp.AWPServer.domain.Order;
import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.util.List;

public interface OrderService {

    Order getById(Long id);

    Order addOrder(Order order);

    List<Order> getAllOrders();

    Order updateOrder(Order order, Long id);

    void deleteOrder(Long id);

    List<Order> getAllOrdersForEmployeeWithId(Long id);
}
