package com.bsuir.awp.AWPServer.service.impl;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import com.bsuir.awp.AWPServer.domain.Order;
import com.bsuir.awp.AWPServer.repository.OrderRepository;
import com.bsuir.awp.AWPServer.service.OrderService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository repository;

    @Override
    public Order getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public Order addOrder(Order order) {
        return repository.save(order);
    }

    @Override
    public List<Order> getAllOrders() {
        return repository.findAll();
    }

    @Override
    public Order updateOrder(Order newOrder, Long id) {
        return repository.findById(id).map(order -> {
            order.setEmployee(newOrder.getEmployee());
            order.setAction(newOrder.getAction());
            order.setCustomer(newOrder.getCustomer());
            order.setProduct(newOrder.getProduct());
            order.setSupplier(newOrder.getSupplier());
            return repository.save(order);
        }).orElseGet(() -> {
            newOrder.setId(id);
            return repository.save(newOrder);
        });
    }

    @Override
    public void deleteOrder(Long id) {
        repository.findById(id).ifPresent(order -> repository.delete(order));
    }

    @Override
    public List<Order> getAllOrdersForEmployeeWithId(Long id) {
        return repository.findByEmployee_id(id);
    }

}
