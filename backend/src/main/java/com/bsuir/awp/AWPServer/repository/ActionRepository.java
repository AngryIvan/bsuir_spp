package com.bsuir.awp.AWPServer.repository;

import com.bsuir.awp.AWPServer.domain.Action;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionRepository extends JpaRepository<Action, Long> {

    boolean existsByName(String name);
}
