package com.bsuir.awp.AWPServer.service;

import java.util.List;
import com.bsuir.awp.AWPServer.domain.Employee;

public interface EmployeeService {

    Employee getById(Long id);

    Employee addEmployee(Employee employee);

    List<Employee> getAllEmployees();

    Employee updateEmployee(Employee employee, Long id);

    boolean deleteEmployee(Long id);

    Employee getByLogin(String login);

}
