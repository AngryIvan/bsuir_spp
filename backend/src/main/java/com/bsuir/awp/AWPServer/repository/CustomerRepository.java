package com.bsuir.awp.AWPServer.repository;

import com.bsuir.awp.AWPServer.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    boolean existsByLogin(String login);
    boolean existsByEmail(String email);
}
