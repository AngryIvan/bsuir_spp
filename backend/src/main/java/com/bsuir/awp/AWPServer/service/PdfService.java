package com.bsuir.awp.AWPServer.service;

import java.io.FileNotFoundException;
import com.itextpdf.text.DocumentException;

public interface PdfService {
    byte[] getAdminReportPdf() throws DocumentException;
    byte[] getReportPdf() throws DocumentException;
}
