package com.bsuir.awp.AWPServer.service.impl;

import com.bsuir.awp.AWPServer.domain.Task;
import com.bsuir.awp.AWPServer.domain.TaskStatus;
import com.bsuir.awp.AWPServer.repository.TaskRepository;
import com.bsuir.awp.AWPServer.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository repository;

    @Override
    public Task getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public Task addTask(Task task) {
        return repository.save(task);
    }

    @Override
    public List<Task> getAllTasks() {
        return repository.findAll();
    }

    @Override
    public List<Task> getAllTasksForEmployeeWithId(Long id) {
        return repository.findByEmployee_id(id);
    }

    @Override
    public Task updateTask(Task newTask, Long id) {
        return repository.findById(id).map(task -> {
            task.setEmployee(newTask.getEmployee());
            task.setDescription(newTask.getDescription());
            task.setStatus(newTask.getStatus());
            return repository.save(task);
        }).orElseGet(() -> {
            newTask.setId(id);
            return repository.save(newTask);
        });
    }

    @Override
    public boolean changeStatus(TaskStatus status, Long id) {
        Optional<Task> optionalTask = repository.findById(id);
        if(optionalTask.isPresent()) {
            optionalTask.get().setStatus(status);
            repository.save(optionalTask.get());
            return true;
        }
        return false;
    }

    @Override
    public void deleteTask(Long id) {
        repository.findById(id).ifPresent(task -> repository.delete(task));
    }
}
