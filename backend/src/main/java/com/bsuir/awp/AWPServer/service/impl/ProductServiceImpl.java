package com.bsuir.awp.AWPServer.service.impl;

import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import com.bsuir.awp.AWPServer.domain.Product;
import com.bsuir.awp.AWPServer.repository.ProductRepository;
import com.bsuir.awp.AWPServer.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;

    @Override
    public Product getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public Product addProduct(Product product) {
        if(!repository.existsByName(product.getName())) {
            return repository.save(product);
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public List<Product> getAllProducts() {
        return repository.findAll();
    }

    @Override
    public Product updateProduct(Product newProduct, Long id) {
        if(!repository.existsByName(newProduct.getName())) {
            return repository.findById(id).map(product -> {
                product.setName(newProduct.getName());
                product.setCost(newProduct.getCost());
                return repository.save(product);
            }).orElseGet(() -> {
                newProduct.setId(id);
                return repository.save(newProduct);
            });
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public void deleteProduct(Long id) {
        repository.findById(id).ifPresent(product -> repository.delete(product));
    }
}
