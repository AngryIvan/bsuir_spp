package com.bsuir.awp.AWPServer.repository;

import com.bsuir.awp.AWPServer.domain.WorkingTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkingTimeRepository extends JpaRepository<WorkingTime, Long> {

}
