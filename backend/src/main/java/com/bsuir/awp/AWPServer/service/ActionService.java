package com.bsuir.awp.AWPServer.service;

import java.util.List;

import com.bsuir.awp.AWPServer.domain.Action;

public interface ActionService {

    Action getById(Long id);

    Action addAction(Action action);

    List<Action> getAllActions();

    Action updateAction(Action action, Long id);

    void deleteAction(Long id);
}
