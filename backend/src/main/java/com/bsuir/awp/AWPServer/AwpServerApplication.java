package com.bsuir.awp.AWPServer;

import org.apache.log4j.Logger;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class AwpServerApplication implements CommandLineRunner {

    private final static Logger LOG = Logger.getLogger(AwpServerApplication.class);

    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication(AwpServerApplication.class);
        sa.setBannerMode(Banner.Mode.OFF);
        sa.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Application started...");
    }

}
