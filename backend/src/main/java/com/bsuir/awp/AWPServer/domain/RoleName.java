package com.bsuir.awp.AWPServer.domain;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}