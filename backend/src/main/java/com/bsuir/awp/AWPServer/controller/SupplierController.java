package com.bsuir.awp.AWPServer.controller;

import com.bsuir.awp.AWPServer.domain.Supplier;
import com.bsuir.awp.AWPServer.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
@RequestMapping("/suppliers")
public class SupplierController {

    @Autowired
    private SupplierService service;

    @GetMapping
    public ResponseEntity<Object> getAllSuppliers() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllSuppliers());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getSupplierById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getById(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PostMapping
    public ResponseEntity<Void> newSupplier(@Valid @RequestBody Supplier supplier){
        try {
            service.addSupplier(supplier);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Supplier with such name already exists", e);
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateSupplierById(@Valid @RequestBody Supplier supplier, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateSupplier(supplier, id));
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Supplier with such name already exists", e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteSupplierById(@PathVariable Long id) {
        service.deleteSupplier(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
