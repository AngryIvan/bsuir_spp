package com.bsuir.awp.AWPServer.controller;

import com.bsuir.awp.AWPServer.domain.Order;
import com.bsuir.awp.AWPServer.dto.FullOrderDto;
import com.bsuir.awp.AWPServer.dto.OrderDto;
import com.bsuir.awp.AWPServer.service.*;
import com.itextpdf.text.DocumentException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.stream.Collectors;

@RestController
public class OrderController {

    private final static Logger LOG = Logger.getLogger(OrderController.class);

    @Autowired
    private OrderService service;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ActionService actionService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProductService productService;

    @Autowired
    private PdfService pdfService;

    @GetMapping("/orders")
    public ResponseEntity<Object> getAllOrders() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllOrders()
                    .stream()
                    .map(this::convertEntityToDto)
                    .collect(Collectors.toList()));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping(value = "/orders", params = "employeeId")
    public ResponseEntity<Object> getAllOrdersWithEmployeeId(@RequestParam Long employeeId) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllOrdersForEmployeeWithId(employeeId)
                    .stream()
                    .map(this::convertEntityToDto)
                    .collect(Collectors.toList()));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(convertEntityToDto(service.getById(id)));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/admin/orders/report")
    public ResponseEntity<Object> getAdminReport() {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(service.getAllOrders()
                                  .stream()
                                  .map(this::convertEntityToFullDto)
                                  .collect(Collectors.toList()));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/admin/orders/report/pdf")
    public ResponseEntity<Object> getAdminReportPdf() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "admin_report_" + LocalDate.now().toString() + ".pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            return new ResponseEntity<>(pdfService.getAdminReportPdf(), headers, HttpStatus.OK);
        }  catch (DocumentException e) {
            LOG.info("PDF EXCEPTION: " + e.toString());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(e.toString());
        }
    }

    @GetMapping("/orders/report/pdf")
    public ResponseEntity<Object> getReportPdf() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            String filename = "report_" + LocalDate.now().toString() + ".pdf";
            headers.setContentDispositionFormData(filename, filename);
            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            return new ResponseEntity<>(pdfService.getReportPdf(), headers, HttpStatus.OK);
        }  catch (DocumentException e) {
            LOG.info("PDF EXCEPTION: " + e.toString());
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(e.toString());
        }
    }

    @PostMapping("/orders")
    public ResponseEntity<Void> newOrder(@RequestBody OrderDto taskDto){
        service.addOrder(convertDtoToEntity(taskDto));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Object> deleteTaskById(@PathVariable Long id) {
        service.deleteOrder(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateTask(@RequestBody OrderDto dto, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(
                    convertEntityToDto(service.updateOrder(convertDtoToEntity(dto), id)));
        } catch (SecurityException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    public Order convertDtoToEntity(OrderDto dto) {
        return Order.builder()
                .id(dto.getId())
                .action(actionService.getById(dto.getActionId()))
                .customer(customerService.getById(dto.getCustomerId()))
                .product(productService.getById(dto.getProductId()))
                .supplier(supplierService.getById(dto.getSupplierId()))
                .employee(employeeService.getById(dto.getEmployeeId()))
                .build();
    }

    public OrderDto convertEntityToDto(Order order) {
        return OrderDto.builder()
                .id(order.getId())
                .actionId(order.getAction().getId())
                .actionName(order.getAction().getName())
                .customerId(order.getCustomer().getId())
                .customerLogin(order.getCustomer().getLogin())
                .employeeId(order.getEmployee().getId())
                .employeeLogin(order.getEmployee().getLogin())
                .productId(order.getProduct().getId())
                .productName(order.getProduct().getName())
                .supplierId(order.getSupplier().getId())
                .supplierName(order.getSupplier().getName())
                .build();
    }

    public FullOrderDto convertEntityToFullDto(Order order) {
        return FullOrderDto.builder()
                .id(order.getId())
                .actionId(order.getAction().getId())
                .actionName(order.getAction().getName())
                .customerId(order.getCustomer().getId())
                .customerLogin(order.getCustomer().getLogin())
                .employeeId(order.getEmployee().getId())
                .employeeLogin(order.getEmployee().getLogin())
                .productId(order.getProduct().getId())
                .productName(order.getProduct().getName())
                .supplierId(order.getSupplier().getId())
                .supplierName(order.getSupplier().getName())
                .creationDate(order.getCreateDate().toString())
                .updateDate(order.getUpdateDateTime().toString())
                .summaryCost(order.getAction().getCost() + order.getProduct().getCost())
                .build();
    }
}
