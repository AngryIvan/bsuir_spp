package com.bsuir.awp.AWPServer.controller;

import com.bsuir.awp.AWPServer.domain.WorkingTime;
import com.bsuir.awp.AWPServer.service.WorkingTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
@RequestMapping("/suppliers")
public class WorkingTimeController {

    @Autowired
    private WorkingTimeService service;

    @GetMapping("/admin/time")
    public ResponseEntity<Object> getAllTime() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllTime());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PostMapping("/api/time")
    public ResponseEntity<Void> addTime(@Valid @RequestBody WorkingTime time){
        try {
            service.addTime(time);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Time item already exists", e);
        }

    }
}
