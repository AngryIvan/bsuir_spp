package com.bsuir.awp.AWPServer.controller;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import com.bsuir.awp.AWPServer.domain.Customer;
import com.bsuir.awp.AWPServer.service.CustomerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @GetMapping
    public ResponseEntity<Object> getAllCustomers() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllCustomers());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "Can't find any customer", e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "Can't find customer with such id", e);
        }
    }

    @PostMapping
    public ResponseEntity<Void> newCustomer(@Valid @RequestBody Customer customer){
        try {
            service.addCustomer(customer);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Customer with such login or email already exists", e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateCustomerById(@Valid @RequestBody Customer customer, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateCustomer(customer, id));
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Customer with such login or email already exists", e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable Long id) {
        service.deleteCustomer(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
