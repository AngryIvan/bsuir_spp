package com.bsuir.awp.AWPServer.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

@Entity
@Data
@Table(name = "customers")
@EqualsAndHashCode(exclude = {"orders"})
@ToString(exclude = {"orders"})
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    public Long id;

    @Length(max = 15, min = 3, message = "Incorrect login length! Min length should be 3 and max should be 15.")
    @Column(length = 15, nullable = false, unique = true)
    private String login;

    @Pattern(regexp = "^(.+)@(.+)$", message = "Incorrect email")
    @Column(length = 25, nullable = false, unique = true)
    private String email;

    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$", message = "Incorrect phone number")
    @Column(length = 20)
    private String phone;

    @JsonIgnore
    @OneToMany(mappedBy = "customer", cascade=CascadeType.ALL)
    private Set<Order> orders;

    @CreationTimestamp
    @Column(name = "create_date", updatable = false)
    private LocalDateTime createDate;
}
