package com.bsuir.awp.AWPServer.repository;

import com.bsuir.awp.AWPServer.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByEmployee_id(Long employeeId);
}
