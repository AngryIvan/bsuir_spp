package com.bsuir.awp.AWPServer.controller;

import com.bsuir.awp.AWPServer.domain.Action;
import com.bsuir.awp.AWPServer.service.ActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
public class ActionController {

    @Autowired
    private ActionService service;

    @GetMapping("/actions")
    public ResponseEntity<Object> getAllActions() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllActions());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "Can't find any action", e);
        }
    }

    @GetMapping("/actions/{id}")
    public ResponseEntity<Object> getActionById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NO_CONTENT, "Can't find action with such id", e);
        }
    }

    @PostMapping("/admin/actions")
    public ResponseEntity<Object> newAction(@Valid @RequestBody Action action){
        try {
            service.addAction(action);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Action with such name already exists", e);
        }
    }

    @PutMapping("/admin/actions/{id}")
    public ResponseEntity<Object> updateActionById(@Valid @RequestBody Action action, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateAction(action, id));
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Action with such name already exists", e);
        }
    }

    @DeleteMapping("/admin/actions/{id}")
    public ResponseEntity<Object> deleteActionById(@PathVariable Long id) {
        service.deleteAction(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
