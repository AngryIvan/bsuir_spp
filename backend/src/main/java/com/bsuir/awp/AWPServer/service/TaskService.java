package com.bsuir.awp.AWPServer.service;

import com.bsuir.awp.AWPServer.domain.Task;
import com.bsuir.awp.AWPServer.domain.TaskStatus;

import java.util.List;

public interface TaskService {

    Task getById(Long id);

    Task addTask(Task task);

    List<Task> getAllTasks();

    Task updateTask(Task task, Long id);

    boolean changeStatus(TaskStatus status, Long id);

    List<Task> getAllTasksForEmployeeWithId(Long id);

    void deleteTask(Long id);
}
