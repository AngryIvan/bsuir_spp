package com.bsuir.awp.AWPServer.service.impl;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;
import com.bsuir.awp.AWPServer.domain.Order;
import com.bsuir.awp.AWPServer.repository.OrderRepository;
import com.bsuir.awp.AWPServer.service.PdfService;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PdfServiceImpl implements PdfService {

    @Autowired
    private OrderRepository repository;

    private final static Logger LOG = Logger.getLogger(PdfServiceImpl.class);

    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);

    @Override
    public byte[] getAdminReportPdf() throws DocumentException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        Document document = new Document();
        PdfWriter.getInstance(document, bout);
        document.open();
        List<Order> orderList = repository.findAll();
        addMetaData(document);
        addAdminContent(document, orderList);
        document.close();
        return bout.toByteArray();
    }

    @Override
    public byte[] getReportPdf() throws DocumentException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        Document document = new Document();
        PdfWriter.getInstance(document, bout);
        document.open();
        List<Order> orderList = repository.findAll();
        addMetaData(document);
        addContent(document, orderList);
        document.close();
        return bout.toByteArray();
    }

    private static void addMetaData(Document document) {
        document.addTitle("Report PDF");
        document.addSubject("AWP");
        document.addKeywords("Java, PDF, iText");
    }

    private static void addContent(Document document, List<Order> orderList) throws DocumentException {
        String date = LocalDate.now().toString().replace('-','.');
        Anchor anchor = new Anchor("Sales report: " + date, catFont);
        document.add(anchor);
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(99);
        Stream.of("Product", "Action", "Summary Cost", "Customer", "Employee", "Supplier")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
        orderList.forEach(order -> {
            table.addCell(order.getProduct().getName());
            table.addCell(order.getAction().getName());
            table.addCell(order.getProduct().getCost() + order.getAction().getCost() + "");
            table.addCell(order.getCustomer().getLogin());
            table.addCell(order.getEmployee().getLogin());
            table.addCell(order.getSupplier().getName());
        });
        document.add(table);
    }

    private static void addAdminContent(Document document, List<Order> orderList) throws DocumentException {
        String date = LocalDate.now().toString().replace('-','.');
        Anchor anchor = new Anchor("Sales report: " + date, catFont);
        document.add(anchor);
        PdfPTable table = new PdfPTable(9);
        table.setWidthPercentage(99);
        Stream.of("Id", "Product", "Action", "Summary Cost", "Customer", "Employee", "Supplier", "Create date", "Update date")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
        orderList.forEach(order -> {
            table.addCell(order.getId().toString());
            table.addCell(order.getProduct().getName());
            table.addCell(order.getAction().getName());
            table.addCell(order.getProduct().getCost() + order.getAction().getCost() + "");
            table.addCell(order.getCustomer().getLogin());
            table.addCell(order.getEmployee().getLogin());
            table.addCell(order.getSupplier().getName());
            table.addCell(order.getCreateDate().toString());
            table.addCell(order.getUpdateDateTime().toString());
        });
        document.add(table);
    }
}
