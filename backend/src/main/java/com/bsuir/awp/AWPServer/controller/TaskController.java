package com.bsuir.awp.AWPServer.controller;

import com.bsuir.awp.AWPServer.domain.Task;
import com.bsuir.awp.AWPServer.dto.TaskDto;
import com.bsuir.awp.AWPServer.service.EmployeeService;
import com.bsuir.awp.AWPServer.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
public class TaskController {

    @Autowired
    private TaskService service;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/tasks")
    public ResponseEntity<Object> getAllTasks() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllTasks()
                    .stream()
                    .map(this::convertEntityToDto)
                    .collect(Collectors.toList()));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

//    @GetMapping(value = "/tasks", params = "employeeId")
//    public ResponseEntity<Object> getAllTasksWithEmployeeId(@RequestParam Long employeeId) {
//        try {
//            return ResponseEntity.status(HttpStatus.OK).body(service.getAllTasksForEmployeeWithId(employeeId)
//                    .stream()
//                    .map(this::convertEntityToDto)
//                    .collect(Collectors.toList()));
//        } catch (EntityNotFoundException e) {
//            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
//        }
//    }

    @GetMapping("/tasks/{id}")
    public ResponseEntity<Object> getAllTasksWithEmployeeId(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllTasksForEmployeeWithId(id)
                    .stream()
                    .map(this::convertEntityToDto)
                    .collect(Collectors.toList()));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PostMapping("/admin/tasks")
    public ResponseEntity<Void> newTask(@Valid @RequestBody TaskDto taskDto){
        service.addTask(convertDtoToEntity(taskDto));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping("/admin/tasks/{id}")
    public ResponseEntity<Object> deleteTaskById(@PathVariable Long id) {
        service.deleteTask(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/admin/tasks/{id}")
    public ResponseEntity<Object> updateTask(@Valid @RequestBody TaskDto dto, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(
                    convertEntityToDto(service.updateTask(convertDtoToEntity(dto), id)));
        } catch (SecurityException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PutMapping("/tasks/{id}")
    public ResponseEntity<Object> updateTaskStatus(@RequestBody TaskDto dto, @PathVariable Long id) {
        return ResponseEntity.status(
                service.changeStatus(dto.getStatus(), id) ? HttpStatus.OK : HttpStatus.NOT_FOUND).build();
    }

    public Task convertDtoToEntity(TaskDto dto) {
        return Task.builder()
                .id(dto.getId())
                .description(dto.getDescription())
                .status(dto.getStatus())
                .employee(employeeService.getById(dto.getEmployeeId()))
                .build();
    }

    public TaskDto convertEntityToDto(Task task) {
        return TaskDto.builder()
                .id(task.getId())
                .description(task.getDescription())
                .status(task.getStatus())
                .employeeId(task.getEmployee().getId())
                .build();
    }
}
