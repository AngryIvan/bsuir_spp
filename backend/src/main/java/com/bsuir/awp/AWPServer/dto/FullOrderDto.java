package com.bsuir.awp.AWPServer.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FullOrderDto {
    private Long id;
    private Long employeeId;
    private Long customerId;
    private Long productId;
    private Long supplierId;
    private Long actionId;
    private Double summaryCost;
    private String employeeLogin;
    private String customerLogin;
    private String productName;
    private String supplierName;
    private String actionName;
    private String creationDate;
    private String updateDate;
}
