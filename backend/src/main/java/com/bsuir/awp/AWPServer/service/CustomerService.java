package com.bsuir.awp.AWPServer.service;

import java.util.List;
import com.bsuir.awp.AWPServer.domain.Customer;

public interface CustomerService {

    Customer getById(Long id);

    Customer addCustomer(Customer customer);

    List<Customer> getAllCustomers();

    Customer updateCustomer(Customer customer, Long id);

    void deleteCustomer(Long id);

}
