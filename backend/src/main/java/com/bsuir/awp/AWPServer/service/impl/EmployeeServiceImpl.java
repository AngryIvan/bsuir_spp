package com.bsuir.awp.AWPServer.service.impl;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import com.bsuir.awp.AWPServer.domain.Customer;
import com.bsuir.awp.AWPServer.domain.Employee;
import com.bsuir.awp.AWPServer.domain.RoleName;
import com.bsuir.awp.AWPServer.repository.EmployeeRepository;
import com.bsuir.awp.AWPServer.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public Employee getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public Employee addEmployee(Employee employee) {
        if(uniqueCheck(employee)) {
            return repository.save(employee);
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public List<Employee> getAllEmployees() {
        return repository.findAll();
    }

    @Override
    public Employee updateEmployee(Employee newEmployee, Long id) {
        if(uniqueCheck(newEmployee)) {
            Optional<Employee> optionalEmployee = repository.findById(id);
            optionalEmployee.ifPresent(employee -> {});
            if(optionalEmployee.isPresent()) {
                if(isRoleNotAdmin(optionalEmployee.get())) {
                    return optionalEmployee.map(employee -> {
                        employee.setLogin(newEmployee.getLogin());
                        employee.setEmail(newEmployee.getEmail());
                        employee.setPhone(newEmployee.getPhone());
                        return repository.save(employee);
                    }).get();
                }
            }
            throw new SecurityException("Access denied!");
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public boolean deleteEmployee(Long id) {
        Optional<Employee> employee = repository.findById(id);
        if(employee.isPresent()) {
            if(isRoleNotAdmin(employee.get())) {
                repository.delete(employee.get());
                return true;
            }
        }
        return false;
    }

    @Override
    public Employee getByLogin(String login) {
        return repository.findByLogin(login).orElseThrow(() -> new EntityNotFoundException(login));
    }


    private boolean isRoleNotAdmin(Employee employee) {
        return employee.getRoles().stream().noneMatch(role -> role.getName().equals(RoleName.ROLE_ADMIN));
    }

    private boolean uniqueCheck(Employee employee) {
        return !repository.existsByLogin(employee.getLogin()) && !repository.existsByEmail(employee.getEmail());
    }

}
