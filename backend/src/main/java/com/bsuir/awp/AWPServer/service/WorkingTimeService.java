package com.bsuir.awp.AWPServer.service;

import com.bsuir.awp.AWPServer.domain.Task;
import com.bsuir.awp.AWPServer.domain.WorkingTime;
import com.bsuir.awp.AWPServer.repository.EmployeeRepository;
import com.bsuir.awp.AWPServer.repository.WorkingTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface WorkingTimeService {

    WorkingTime addTime(WorkingTime time);

    List<WorkingTime> getAllTime();
}
