package com.bsuir.awp.AWPServer.dto;

import com.bsuir.awp.AWPServer.domain.TaskStatus;
import lombok.*;

import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskDto {
    private Long id;
    @Size(min = 10, max = 50, message = "Min length should be 10 and max should be 50")
    private String description;
    private TaskStatus status;
    private Long employeeId;
}
