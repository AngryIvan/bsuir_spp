package com.bsuir.awp.AWPServer.controller;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import com.bsuir.awp.AWPServer.domain.Product;
import com.bsuir.awp.AWPServer.service.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final static Logger LOG = Logger.getLogger(OrderController.class);

    @Autowired
    private ProductService service;

    @GetMapping
    public ResponseEntity<Object> getAllProducts() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllProducts());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getById(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PostMapping
    public ResponseEntity<Void> newProduct(@Valid @RequestBody Product product){
        try {
            service.addProduct(product);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Product with such name already exists", e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateProductById(@Valid @RequestBody Product product, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateProduct(product, id));
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Product with such name already exists", e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteProductById(@PathVariable Long id) {
        service.deleteProduct(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
