package com.bsuir.awp.AWPServer.controller;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import com.bsuir.awp.AWPServer.domain.Employee;
import com.bsuir.awp.AWPServer.domain.Role;
import com.bsuir.awp.AWPServer.dto.EmployeeDto;
import com.bsuir.awp.AWPServer.service.EmployeeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Collectors;

@RestController
public class EmployeeController {

    private final static Logger LOG = Logger.getLogger(OrderController.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EmployeeService service;

    @GetMapping("/employees")
    public ResponseEntity<Object> getAllEmployees() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(
                    service.getAllEmployees()
                            .stream()
                            .map(this::convertEntityToDto)
                            .collect(Collectors.toList()));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(convertEntityToDto(service.getById(id)));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/employees/login")
    public ResponseEntity<Object> login() {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            return ResponseEntity.status(HttpStatus.OK).body(service.getByLogin(auth.getName()));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @PostMapping("/admin/employees")
    public ResponseEntity<Void> newEmployee(@Valid @RequestBody EmployeeDto employeeDto){
        try {
            service.addEmployee(convertDtoToEntity(employeeDto));
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Employee with such login or email already exists", e);
        }
    }

    @DeleteMapping("/admin/employees/{id}")
    public ResponseEntity<Void> deleteEmployeeById(@PathVariable Long id) {
        return ResponseEntity.status(service.deleteEmployee(id) ? HttpStatus.OK : HttpStatus.UNAUTHORIZED).build();
    }

    @PutMapping("/admin/employees/{id}")
    public ResponseEntity<Object> updateEmployee(@Valid @RequestBody EmployeeDto dto, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(convertEntityToDto(service.updateEmployee(
                    Employee.builder()
                            .login(dto.getLogin())
                            .email(dto.getEmail())
                            .phone(dto.getPhone())
                            .build(), id)));
        } catch (SecurityException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Employee with such login or email already exists", e);
        }
    }

    @GetMapping("/employees/{id}/time")
    public ResponseEntity<Object> getTimeEmployeeById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(convertEntityToDto(service.getById(id)));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    public Employee convertDtoToEntity(EmployeeDto dto) {
        return Employee.builder()
                .login(dto.getLogin())
                .email(dto.getEmail())
                .password(passwordEncoder.encode(dto.getPassword()))
                .phone(dto.getPhone())
                .build();
    }

    public EmployeeDto convertEntityToDto(Employee employee) {
        return EmployeeDto.builder()
                .email(employee.getEmail())
                .login(employee.getLogin())
                .phone(employee.getPhone())
                .id(employee.getId())
                .roles(employee.getRoles().stream().map(Role::getName).collect(Collectors.toList()))
                .build();
    }

}
