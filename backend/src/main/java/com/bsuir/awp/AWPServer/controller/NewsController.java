package com.bsuir.awp.AWPServer.controller;

import com.bsuir.awp.AWPServer.domain.News;
import com.bsuir.awp.AWPServer.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
public class NewsController {

    @Autowired
    private NewsService service;

    @GetMapping("/news")
    public ResponseEntity<Object> getAllNews() {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getAllNews());
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @GetMapping("/news/{id}")
    public ResponseEntity<Object> getNewsById(@PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.getById(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PostMapping("/admin/news")
    public ResponseEntity<Void> createNews(@Valid @RequestBody News news){
        try {
            service.addNews(news);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "News with such title already exists", e);
        }

    }

    @PutMapping("/admin/news/{id}")
    public ResponseEntity<Object> updateNewsById(@Valid @RequestBody News news, @PathVariable Long id) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(service.updateNews(news, id));
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "News with such title already exists", e);
        }
    }

    @DeleteMapping("/admin/news/{id}")
    public ResponseEntity<Object> deleteNewsById(@PathVariable Long id) {
        service.deleteNews(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
