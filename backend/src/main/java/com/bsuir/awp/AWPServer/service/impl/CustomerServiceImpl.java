package com.bsuir.awp.AWPServer.service.impl;

import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import com.bsuir.awp.AWPServer.domain.Customer;
import com.bsuir.awp.AWPServer.repository.CustomerRepository;
import com.bsuir.awp.AWPServer.service.CustomerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final static Logger LOG = Logger.getLogger(CustomerServiceImpl.class);

    @Autowired
    private CustomerRepository repository;

    @Override
    public Customer getById(Long id) throws EntityNotFoundException {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public Customer addCustomer(Customer customer) {
        if(uniqueCheck(customer)) {
            return repository.save(customer);
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public List<Customer> getAllCustomers() {
        return repository.findAll();
    }

    @Override
    public Customer updateCustomer(Customer newCustomer, Long id) throws EntityNotFoundException {
        if(uniqueCheck(newCustomer)) {
            return repository.findById(id).map(customer -> {
                customer.setLogin(newCustomer.getLogin());
                customer.setEmail(newCustomer.getEmail());
                customer.setPhone(newCustomer.getPhone());
                return repository.save(customer);
            }).orElseGet(() -> {
                newCustomer.setId(id);
                return repository.save(newCustomer);
            });
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public void deleteCustomer(Long id) {
        repository.findById(id).ifPresent(customer -> repository.delete(customer));
    }

    private boolean uniqueCheck(Customer customer) {
        return !repository.existsByLogin(customer.getLogin()) && !repository.existsByEmail(customer.getEmail());
    }
}
