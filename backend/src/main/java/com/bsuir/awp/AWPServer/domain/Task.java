package com.bsuir.awp.AWPServer.domain;

import lombok.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Table(name = "tasks")
@ToString(exclude = {"employee"})
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "task_id")
    private Long id;

    @Column(length = 50, nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 25)
    private TaskStatus status;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name="employee_id", nullable=false)
    private Employee employee;

    @CreationTimestamp
    @Column(name = "create_date", updatable = false)
    private LocalDateTime createDate;

    @UpdateTimestamp
    @Column(name = "update_date", insertable = false)
    private Timestamp updateDateTime;
}
