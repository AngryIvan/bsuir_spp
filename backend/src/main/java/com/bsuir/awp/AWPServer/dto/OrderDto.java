package com.bsuir.awp.AWPServer.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    private Long id;
    private Long employeeId;
    private Long customerId;
    private Long productId;
    private Long supplierId;
    private Long actionId;
    private String employeeLogin;
    private String customerLogin;
    private String productName;
    private String supplierName;
    private String actionName;
}
