package com.bsuir.awp.AWPServer.service;

import com.bsuir.awp.AWPServer.domain.Supplier;

import java.util.List;

public interface SupplierService {

    Supplier getById(Long id);

    Supplier addSupplier(Supplier supplier);

    List<Supplier> getAllSuppliers();

    Supplier updateSupplier(Supplier supplier, Long id);

    void deleteSupplier(Long id);
}
