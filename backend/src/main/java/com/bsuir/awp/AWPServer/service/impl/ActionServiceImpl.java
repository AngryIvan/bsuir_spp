package com.bsuir.awp.AWPServer.service.impl;

import com.bsuir.awp.AWPServer.domain.Action;
import com.bsuir.awp.AWPServer.repository.ActionRepository;
import com.bsuir.awp.AWPServer.service.ActionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ActionServiceImpl implements ActionService {


    private final static Logger LOG = Logger.getLogger(ActionServiceImpl.class);

    @Autowired
    private ActionRepository repository;

    @Override
    public Action getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(id.toString()));
    }

    @Override
    public Action addAction(Action action) throws UnsupportedOperationException {
        if(!repository.existsByName(action.getName())) {
            return repository.save(action);
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public List<Action> getAllActions() {
        return repository.findAll();
    }

    @Override
    public Action updateAction(Action newAction, Long id) {
        if(!repository.existsByName(newAction.getName())) {
            return repository.findById(id).map(action -> {
                action.setName(newAction.getName());
                action.setCost(newAction.getCost());
                return repository.save(action);
            }).orElseGet(() -> {
                newAction.setId(id);
                return repository.save(newAction);
            });
        }
        else throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAction(Long id) {
        repository.findById(id).ifPresent(action -> repository.delete(action));
    }
}
