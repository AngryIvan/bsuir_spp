package com.bsuir.awp.AWPServer.service;

import java.util.List;
import com.bsuir.awp.AWPServer.domain.Product;

public interface ProductService {

    Product getById(Long id);

    Product addProduct(Product product);

    List<Product> getAllProducts();

    Product updateProduct(Product product, Long id);

    void deleteProduct(Long id);
}
