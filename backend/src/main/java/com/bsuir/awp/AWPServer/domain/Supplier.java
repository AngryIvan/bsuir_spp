package com.bsuir.awp.AWPServer.domain;

import lombok.Data;

import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "suppliers")
@Data
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "supplier_id")
    public Long id;

    @Size(min = 5, max = 25, message = "Min length should be 5 and max should be 25")
    @Column(length = 25, nullable = false, unique = true)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "supplier", cascade=CascadeType.ALL)
    private Set<Order> orders;
}
