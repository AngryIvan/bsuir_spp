package com.bsuir.awp.AWPServer.domain;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "employees")
@EqualsAndHashCode(exclude = {"password", "orders", "tasks"})
@ToString(exclude = {"password", "orders", "tasks"})
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    public Long id;

    @Column(length = 15, nullable = false, unique = true)
    private String login;

    @JsonIgnore
    @Column(length = 100, nullable = false)
    private String password;

    @Column(length = 25, nullable = false, unique = true)
    private String email;

    @Column(length = 15)
    private String phone;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "employee_roles",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    @JsonIgnore
    @OneToMany(mappedBy = "employee", cascade=CascadeType.ALL)
    private Set<Order> orders;

    @JsonIgnore
    @OneToMany(mappedBy = "employee", cascade=CascadeType.ALL)
    private Set<Task> tasks;

    @CreationTimestamp
    @Column(name = "create_date", updatable = false)
    private LocalDateTime createDate;

}
