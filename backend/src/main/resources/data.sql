INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');

INSERT INTO employees (login, password, email, create_date) VALUES ('user_1', '$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', 'testuser1@gmail.com', '2019-06-06 23:59:00');
INSERT INTO employees (login, password, email, create_date) VALUES ('user_2', '$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', 'testuser2@gmail.com', '2019-06-06 23:59:00');
INSERT INTO employees (login, password, email, create_date) VALUES ('Test_employee_3', '$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', 'testuser3@gmail.com', '2019-06-06 23:59:00');
INSERT INTO employees (login, password, email, create_date) VALUES ('Test_employee_4', '$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', 'testuser4@gmail.com', '2019-06-06 23:59:00');
INSERT INTO employees (login, password, email, create_date) VALUES ('Test_employee_5', '$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', 'testuser5@gmail.com', '2019-06-06 23:59:00');

INSERT INTO customers (login, email, phone, create_date) VALUES ('fireMonkey', 'fireMonkey@gmail.com','+37529 9829752', '2019-05-06 23:59:00');
INSERT INTO customers (login, email, phone, create_date) VALUES ('MineCraftJake', 'cragtjake@hotmail.com','+37533 7788223', '2019-05-08 23:59:00');

INSERT INTO actions (name, cost) VALUES ('purchase', '0');
INSERT INTO actions (name, cost) VALUES ('purchase with delivery', '10');
INSERT INTO actions (name, cost) VALUES ('strings replacement', '10');

INSERT INTO suppliers (name) VALUES ('Supplier 1');
INSERT INTO suppliers (name) VALUES ('Supplier 2');


INSERT INTO products (name, cost) VALUES ('Jackson Dinky JS12', '500');

INSERT INTO orders (employee_id, customer_id, action_id, supplier_id, product_id, create_date, update_date) VALUES (1, 1, 1, 1,1, '2019-06-06 23:59:00', '2019-06-06 23:59:00');

INSERT INTO employee_roles (employee_id, role_id) VALUES (1, 2);
INSERT INTO employee_roles (employee_id, role_id) VALUES (2, 1);
INSERT INTO employee_roles (employee_id, role_id) VALUES (3, 1);
INSERT INTO employee_roles (employee_id, role_id) VALUES (4, 1);
INSERT INTO employee_roles (employee_id, role_id) VALUES (5, 1);

INSERT INTO tasks (description, status, employee_id, create_date) VALUES ('Do Chored', 'TODO_STATUS', 1, '2019-06-06 23:59:00');
INSERT INTO tasks (description, status, employee_id, create_date) VALUES ('Make Coffee', 'TODO_STATUS', 2, '2019-06-06 23:59:00');

INSERT INTO news (title, description, create_date) VALUES ('Breaking news!', 'Everything is all right', '2019-05-06 23:59:00');
INSERT INTO news (title, description, create_date) VALUES ('Not breaking news!', 'Everything is not all right', '2019-05-06 23:59:00');


INSERT INTO working_time (employee_id, create_date, time_seconds) VALUES (1, '2019-05-06 23:59:00', 540);

