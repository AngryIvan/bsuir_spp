import React, { Component } from 'react';
import { BrowserRouter, Route  } from "react-router-dom";
import './App.css';
import 'antd/dist/antd.css';

import Header from "./components/layout/header/Header";
import Actions from "./components/actions/Actions";
import Customers from "./components/customers/Customers";
import Employees from "./components/employees/Employees";
import News from "./components/news/News";
import Orders from "./components/orders/Orders";
import Products from "./components/products/Products";
import Suppliers from "./components/suppliers/Suppliers";
import Tasks from "./components/tasks/Tasks";
import WorkingTime from "./components/workingTime/WorkingTIme";

class App extends Component {

  async componentWillMount() {
    const response = await fetch('http://localhost:8080/employees/login');
    const json = await response.json();
    this.setState({isAdmin: json.roles[0].name})
    await localStorage.setItem('isAdmin', json.roles[0].name);
    await localStorage.setItem('userId', json.id)
  }
  state =  {
    isAdmin: false,
  }

  render() {
    const { isAdmin } = this.state;
    return (
      <BrowserRouter>
        <div className="App">
          <Header/>
          <div className="Routes">
            
            <Route exact path="/actions" component={(props) => <Actions {...props} isAdmin={isAdmin}/>}/>
            <Route exact path="/customers" component={(props) => <Customers {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/employees" component={(props) => <Employees {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/news" component={(props) => <News {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/orders" component={(props) => <Orders {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/products" component={(props) => <Products {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/suppliers" component={(props) => <Suppliers {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/tasks" component={(props) => <Tasks {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/time" component={(props) => <WorkingTime {...props} isAdmin={isAdmin}/>} />
            <Route exact path="/" component={(props) => <News {...props} isAdmin={isAdmin}/>} />

          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
