import React, { Component } from 'react';
import { List, Card, Button, Spin, Input } from 'antd';

export default class Tasks extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.onTaskClick = this.onTaskClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }


  componentDidMount() {
    this.getData();
  }

  async getData() {
    const response = await fetch(`http://localhost:8080/tasks/${localStorage.getItem('userId')}`);
    const json = await response.json();
    await this.setState({ data: json });
  }

  async onTaskClick(item) {
    await fetch(`http://localhost:8080/tasks/${item.id}`, {
      method: "PUT",
      body: JSON.stringify({ "status": "DONE_STATUS"}),
      headers:{
        "Content-Type": "application/json"
      }
    }).then(res => JSON.stringify(res))
      .catch(error => console.error('Error:', error));
    await this.getData();
  }

  async handleSubmit() {
    await fetch(`http://localhost:8080/admin/tasks`, {
      method: "POST",
      body: JSON.stringify({  
        "description": this.state.description,
        "status": "TODO_STATUS",
        "employeeId": this.state.employeeId}),
      headers:{
        "Content-Type": "application/json"
      }
    }).catch(error => console.error('Error:', error));
    await this.getData();

  }

  handleChange (evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  render() {
    if (this.state)
    return (
      <>
      {this.props.isAdmin === "ROLE_ADMIN" && <form>
          <Input name="description" placeholder="Description" onChange={this.handleChange}  />
          <Input name="employeeId" placeholder="Employee ID" onChange={this.handleChange}  />
        <Button type="primary" block onClick={this.handleSubmit}>Create Task</Button>
      </form>}
      <List
        grid={{ gutter: 16, column: 4 }}
        dataSource={this.state.data}
        renderItem={item => (
          <List.Item>
            <Card
              title={`${item.id}: ${item.status === "TODO_STATUS" ? "Pending" : "Done" }`}
              onClick={ () => this.onTaskClick(item) }
            >
              {item.description}
            </Card>
          </List.Item>
        )}
      />
      </>
  )
  
  return (<Spin size="large" />);
  }
}


