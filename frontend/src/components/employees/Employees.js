import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';
import uuid from 'uuid'

export default class Employees extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const response = await fetch('http://localhost:8080/employees')
    const json = await response.json();
    await this.setState({ data: json });
  }

  // async handleSubmit() {
  //   await fetch(`http://localhost:8080/admin/employees`, {
  //     method: "POST",
  //     body: JSON.stringify({
  //       "login": this.state.login,
  //       "password": this.state.password,
  //       "email": this.state.email,
  //       "phone": this.state.phone}),
  //     headers:{
  //       "Content-Type": "application/json"
  //     }
  //   }).then(res => JSON.stringify(res))
  //     .catch(error => console.error('Error:', error));
  //   await this.getData();
  // }
  //
  // handleChange (evt) {
  //   this.setState({ [evt.target.name]: evt.target.value });
  // }

//{"id":1,"login":"user_1","password":null,"email":"testuser1@gmail.com","phone":null,"roles":["ROLE_ADMIN"]}
  render() {
    if (this.state) 
    return (
      <>
        {/*{this.props.isAdmin === "ROLE_ADMIN" && <form>*/}
        {/*  <Input name="login" placeholder="Login" onChange={this.handleChange}  />*/}
        {/*  <Input name="email" placeholder="Email" onChange={this.handleChange}  />*/}
        {/*  <Input name="password" placeholder="Password" onChange={this.handleChange}  />*/}
        {/*  <Input name="phone" placeholder="Phone" onChange={this.handleChange}  />*/}
        {/*  <Button type="primary" block onClick={this.handleSubmit}>Create employee</Button>*/}
        {/*</form> }*/}
        <List
          header={<div>Employees</div>}
          bordered
          dataSource={this.state.data}
          renderItem={employee => (
          <List.Item>
            {<strong>{employee.id}: {employee.login} </strong>} <br/>
            <ul>
              <li><strong> Email: </strong>{employee.email}  </li>
              <li><strong> Phone: </strong>{employee.phone} </li>
              <li><strong> Roles: </strong> {employee.roles.map(role => <span key={uuid.v4}>{role === 'ROLE_ADMIN' ? 'Admin' : 'None'}</span>)}</li>
            </ul>
          </List.Item>
        )}
        />
      </>
  )
  return (<Spin size="large" />);
  }
}


