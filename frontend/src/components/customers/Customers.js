import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';

export default class Customers extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const response = await fetch('http://localhost:8080/customers')
    const json = await response.json();
    await this.setState({ data: json });
  }

  async handleSubmit() {
    await fetch(`http://localhost:8080/customers`, {
      method: "POST",
      body: JSON.stringify({  
        "login": this.state.login,
        "email": this.state.email,
        "phone": this.state.phone}),
      headers:{
        "Content-Type": "application/json"
      }
    }).then(res => JSON.stringify(res))
      .catch(error => console.error('Error:', error));
    await this.getData();
  }

  handleChange (evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }


  render() {
    if (this.state) 
    return (
      <>
        {this.props.isAdmin === "ROLE_ADMIN" && <form>
          <Input name="login" placeholder="Login" onChange={this.handleChange}  />
          <Input name="email" placeholder="Email" onChange={this.handleChange}  />
          <Input name="phone" placeholder="Phone" onChange={this.handleChange}  />
          <Button type="primary" block onClick={this.handleSubmit}>Create customer</Button>
        </form>}
        <List
          header={<div>Customers</div>}
          bordered
          dataSource={this.state.data}
          renderItem={customer => (
          <List.Item>
            {<strong>{customer.id}: {customer.login} </strong>} <br/>
            <ul>
              <li><strong> Email: </strong>{customer.email}  </li>
              <li><strong> Phone: </strong>{customer.phone} </li>
              <li><strong> Creation Date: </strong>{customer.createDate[1]}-{customer.createDate[2]}-{customer.createDate[0]}</li>
            </ul>
          </List.Item>
        )}
        />
      </>
  )
  return (<Spin size="large" />);
  }
}


