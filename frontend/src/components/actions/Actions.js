import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';

export default class Actions extends Component {

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getData();

  }

  async getData() {
    const response = await fetch('http://localhost:8080/actions')
    const json = await response.json();
    await this.setState({ data: json });

  }

  async handleSubmit() {
    await fetch(`http://localhost:8080/admin/actions`, {
      method: "POST",
      body: JSON.stringify({
        "name": this.state.name,
        "cost": this.state.cost}),
      headers:{
        "Content-Type": "application/json"
      }
    }).then(res => JSON.stringify(res))
      .catch(error => console.error('Error:', error));
    await this.getData();
  }

  handleChange (evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }
  
//{"id":1,"name":"purchase","cost":0.0}

  render() {
    if (this.state) 
    return (
      <>
        {this.props.isAdmin === "ROLE_ADMIN" && <form>
          <Input name="name" placeholder="Name" onChange={this.handleChange}  allowClear/>
          <Input name="cost" placeholder="Cost" onChange={this.handleChange}  allowClear/>
          <Button type="primary" block onClick={this.handleSubmit}>Create service</Button>
        </form> }
        <List
          header={<div>Services</div>}
          bordered
          dataSource={this.state.data}
          renderItem={action => (
          <List.Item>
            {<strong>{action.id}: {action.name} </strong>} <br/>
            <ul>
              <li><strong> Cost: </strong>{action.cost}  </li>
          </ul>
          </List.Item>
        )}
        />
      </>
  )
  return (<Spin size="large" />);
  }
}


