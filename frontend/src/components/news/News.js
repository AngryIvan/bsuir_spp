import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';

export default class News extends Component {
  
  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const response = await fetch('http://localhost:8080/news')
    const json = await response.json();
    await this.setState({ data: json });
  }

  async handleSubmit() {
    await fetch(`http://localhost:8080/admin/news`, {
      method: "POST",
      body: JSON.stringify({
        "title": this.state.title,
        "description": this.state.description}),
      headers:{
        "Content-Type": "application/json"
      }
    }).then(res => JSON.stringify(res))
      .catch(error => console.error('Error:', error));
    await this.getData();
  }

  handleChange (evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

//[{"id":1,"title":"Breaking news!","description":"Everything is all right","createDate":null}]
  
  render() {
    if (this.state) 
    return (
      <>
        {this.props.isAdmin === "ROLE_ADMIN" && <form>
          <Input name="title" placeholder="Title" onChange={this.handleChange}  allowClear/>
          <Input name="description" placeholder="Description" onChange={this.handleChange}  allowClear/>
          <Button type="primary" block onClick={this.handleSubmit}>Create news</Button>
        </form> }
        <List
          header={<div>News</div>}
          bordered
          dataSource={this.state.data}
          renderItem={news => (
          <List.Item>
            {<strong>{news.id}: {news.title} </strong>} <br/>
            <ul>
              <li><strong> Description: </strong>{news.description}  </li>
              <li><strong> Creation Date: </strong>{news.createDate[1]}-{news.createDate[2]}-{news.createDate[0]}</li>
            </ul>
          </List.Item>
        )}
        />
      </>
  )
  return (<Spin size="large" />);
  }
}


