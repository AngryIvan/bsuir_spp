import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';

export default class Suppliers extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const response = await fetch('http://localhost:8080/suppliers')
    const json = await response.json();
    await this.setState({ data: json });
  }

  async handleSubmit() {
    await fetch(`http://localhost:8080/suppliers`, {
      method: "POST",
      body: JSON.stringify({
        "name": this.state.name,
        }),
      headers:{
        "Content-Type": "application/json"
      }
    }).then(res => JSON.stringify(res))
      .catch(error => console.error('Error:', error));
    await this.getData();
  }

  handleChange (evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

//{"id":1,"name":"Supplier 1"}

  render() {
    if (this.state) 
    return (
      <>
        {this.props.isAdmin === "ROLE_ADMIN" && <form>
          <Input name="name" placeholder="Name" onChange={this.handleChange}  allowClear/>
          <Button type="primary" block onClick={this.handleSubmit}>Create news</Button>
        </form> }
        <List
          header={<div>Suppliers</div>}
          bordered
          dataSource={this.state.data}
          renderItem={supplier => (
          <List.Item>
            {<strong>{supplier.id}: {supplier.name} </strong>} <br/>
          </List.Item>
        )}
        />
      </>
  )
  return (<Spin size="large" />);
  }
}


