import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';

export default class News extends Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await fetch('http://localhost:8080/orders')
        const json = await response.json();
        await this.setState({ data: json });
    }

    async handleSubmit() {
        await fetch(`http://localhost:8080/orders`, {
            method: "POST",
            body: JSON.stringify({
                "employeeId": this.state.employeeId,
                "customerId": this.state.customerId,
                "productId": this.state.productId,
                "supplierId": this.state.supplierId,
                "actionId": this.state.actionId
            }),
            headers:{
                "Content-Type": "application/json"
            }
        }).then(res => JSON.stringify(res))
            .catch(error => console.error('Error:', error));
        await this.getData();
    }

    handleChange (evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }

    getReport = async () => {
        await fetch('http://localhost:8080/orders/report/pdf')
            .then((response) => response.blob())
            .then((blob) => this.download(blob, 'report.pdf'))
    }

    getAdminReport = async () => {
        await fetch('http://localhost:8080/admin/orders/report/pdf')
            .then((response) => response.blob())
            .then((blob) => this.download(blob, 'adminReport.pdf'))
    }


    download (blob, fileName) {
        const url = window.URL.createObjectURL(new Blob([blob]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
    }

    // async onOrderClick(item) {
    //     this.setState({
    //         employeeId: item.employeeId,
    //         customerId: item.customerId,
    //         productId: item.productId,
    //         supplierId: item.supplierId,
    //         actionId: item.actionId
    //     })
    //     await fetch(`http://localhost:8080/order/${item.id}`, {
    //         method: "PUT",
    //         body: JSON.stringify({
    //             "employeeId": this.state.employeeId,
    //             "customerId": this.state.customerId,
    //             "productId": this.state.productId,
    //             "supplierId": this.state.supplierId,
    //             "actionId": this.state.actionId
    //         }),
    //         headers:{
    //             "Content-Type": "application/json"
    //         }
    //     }).then(res => JSON.stringify(res))
    //         .catch(error => console.error('Error:', error));
    //     await this.getData();
    // }

//[{"id":1,"title":"Breaking news!","description":"Everything is all right","createDate":null}]

    render() {
        if (this.state)
            return (
                <>
                    <form>
                        <Input name="employeeId" placeholder="Employee ID" onChange={this.handleChange}  allowClear/>
                        <Input name="customerId" placeholder="Customer ID" onChange={this.handleChange}  allowClear/>
                        <Input name="productId" placeholder="Product ID" onChange={this.handleChange}  allowClear/>
                        <Input name="supplierId" placeholder="Supplier ID" onChange={this.handleChange}  allowClear/>
                        <Input name="actionId" placeholder="Service ID" onChange={this.handleChange}  allowClear/>
                        <Button type="primary" block onClick={this.handleSubmit}>Create order</Button>
                    </form>
                    {localStorage.getItem('isAdmin') === "ROLE_ADMIN" &&
                        <Button type="primary" block onClick={this.getAdminReport}>
                            Admin PDF Report
                        </Button>
                    }
                    <br/><br/>
                    <Button type="primary" block
                            onClick={this.getReport}>
                        Regular PDF Report
                    </Button><br/><br/>
                    <List
                        header={<div>Order</div>}
                        bordered
                        dataSource={this.state.data}
                        renderItem={order => (
                            <List.Item>
                                {<strong>{order.id}: {order.login} </strong>} <br/>
                                <ul>
                                    <li><strong> Customer Login: </strong>{order.customerLogin}  </li>
                                    <li><strong> Product: </strong>{order.productName} </li>
                                    <li><strong> Service: </strong>{order.actionName} </li>
                                    <li><strong> Supplier: </strong>{order.supplierName} </li>
                                </ul>
                            </List.Item>
                        )}
                    />
                </>
            )
        return (<Spin size="large" />);
    }
}

