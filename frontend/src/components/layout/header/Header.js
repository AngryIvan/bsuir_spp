import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Menu, Dropdown, Icon, Button } from 'antd';
import './Header.css';

const menu = (
  <Menu>
    <Menu.Item>
      <Link to="/actions">
        <span>{'Services'}</span>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/customers">
        <span>{'Customers'}</span>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/employees">
        <span>{'Employees'}</span>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/news">
        <span>{'News'}</span>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/orders">
        <span>{'Orders'}</span>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/products">
        <span>{'Products'}</span>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/suppliers">
        <span>{'Suppliers'}</span>
      </Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="/tasks">
        <span>{'Tasks'}</span>
      </Link>
    </Menu.Item>
      <Menu.Item>
          <Link to="/time">
              <span>{'Time'}</span>
          </Link>
      </Menu.Item>

  </Menu>
);

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: this.props.date};
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState(prevState => ({
            date: prevState.date + 1
        }))
    }

    render() {
        return (
            <div>
                <h2 style={{color: 'white'}}>Session: {(Math.round(this.props.date / 60) )} minutes</h2>
            </div>
        );
    }
}
export default class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {date: 0};
    }


    async logoutButton () {
        localStorage.isAdmin = undefined;
        await fetch('http://localhost:8080/logout');
        this.postTime()
        setTimeout("location.reload(true);",100)
    }

    async postTime() {
        await fetch(`http://localhost:8080/api/time`, {
            method: "POST",
            body: JSON.stringify({
                "employeeId": localStorage.getItem('userId'),
                "timeSeconds": this.state.date
            }),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => JSON.stringify(res))
            .catch(error => console.error('Error:', error));
        await this.getData();
    }


    render() {
        return (
            <div className="head-wrapper">
                <header className="main-header">
                    <div>
                        <Link to={'/'}>
                            <h1 className="logo">AWP BSUIR</h1>
                        </Link>
                    </div>
                    <Clock date={this.state.date}/>
                    <Button onClick={this.logoutButton}>
                        <Link to={'/'}>
                            Logout
                        </Link>
                    </Button>
                    <Dropdown overlay={menu}>
                      <span className="ant-dropdown-link">
                        Navigation <Icon type="down"/>
                      </span>
                    </Dropdown>
                </header>
            </div>
        )
    }
}
