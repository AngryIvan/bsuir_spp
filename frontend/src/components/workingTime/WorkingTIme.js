import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';

export default class WorkingTime extends Component {

    componentDidMount() {
        this.getData();
    }

    async getData() {
        const response = await fetch(`http://localhost:8080/admin/`);
        const json = await response.json();
        await this.setState({ data: json });
    }


    render() {
        if (this.state)
            return (
                <>
                    <List
                        header={<div>Time</div>}
                        bordered
                        dataSource={this.state.data}
                        renderItem={item => (
                            <List.Item>
                                {<strong>{item.id}: {item.employee} {item.workDate}; Time: {(Math.round(item.timeSeconds / 60) )}</strong>} <br/>
                            </List.Item>
                        )}
                    />
                </>
            )

        return (<Spin size="large" />);
    }
}


