import React, { Component } from 'react';
import { List, Button, Spin, Input } from 'antd';

export default class Products extends Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    const response = await fetch('http://localhost:8080/products')
    const json = await response.json();
    await this.setState({ data: json });
  }

  async handleSubmit() {
    await fetch(`http://localhost:8080/products`, {
      method: "POST",
      body: JSON.stringify({
        "name": this.state.name,
        "cost": this.state.cost}),
      headers:{
        "Content-Type": "application/json"
      }
    }).then(res => JSON.stringify(res))
      .catch(error => console.error('Error:', error));
    await this.getData();
  }

  handleChange (evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }
//[{"id":1,"name":"Jackson Dinky JS12","cost":500.0}]

  render() {
    if (this.state) 
    return (
      <>
        <form>
          <Input name="name" placeholder="Title" onChange={this.handleChange}  allowClear/>
          <Input name="cost" placeholder="Price" onChange={this.handleChange}  allowClear/>
          <Button type="primary" block onClick={this.handleSubmit}>Create news</Button>
        </form>
        <List
          header={<div>Products</div>}
          bordered
          dataSource={this.state.data}
          renderItem={product => (
          <List.Item>
            {<strong>{product.id}: {product.name} </strong>} <br/>
            <ul>
              <li><strong> Cost: </strong>{product.cost}  </li>
            </ul>
          </List.Item>
        )}
        />
      </>
  )
  return (<Spin size="large" />);
  }
}


